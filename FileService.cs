﻿using System;
using System.IO;
using System.Linq;

namespace File_Manager
{
    // The file service handles requests for information about files and directories.
    static class FileService
    {
        private const string resourcePath = @"..\..\..\resources";

        // Returns a list of all the files in the resources folder
        // If extension is provided, only files that match it are returned.
        public static string[] GetResourseNames(string extension = "")
        {
            // Make sure no partial matches are made, by adding leading dot, if it is missing.
            if (extension.Length > 0 && extension[0] != '.') extension = '.' + extension;

            try
            {
                // Return an array of all the file names in the resources folder, optionally filtered by extension.
                return Directory.GetFiles(resourcePath)
                    .Select(path => path.Split('\\').Last())
                    .Where(filename => filename.EndsWith(extension))
                    .ToArray();
            }
            // If the directory does not exist, let the user know and return an empty array.
            catch (DirectoryNotFoundException)
            {
                ConsoleService.DisplayMessage($"The resources directory does not exist.");
                return new string[] {};
            }
        }

        // Returns a list of unique file extensions present in the resource folder,
        // sorted alphabetically.
        public static string[] GetExtensions()
        {
            string[] Extensions = GetResourseNames()
                .Select(name => name.Split('.').Last())
                .Distinct().ToArray();

            Array.Sort(Extensions);
            return Extensions;
        }

        // Creates and returns a list of information about the given file.
        // This includes file name, file size and number of lines.
        public static string[] GetFileInformation(string fileName)
        {
            string filePath = Path.Combine(resourcePath, fileName);

            try
            {
                string FileName = "File name: \t\t" + fileName;
                string FileSize = "File size (bytes): \t" + new FileInfo(filePath).Length.ToString();
                string NumLines = "Number of lines: \t" + File.ReadAllLines(filePath).Length.ToString();

                return new string[] { FileName, FileSize, NumLines };
            }
            catch (Exception e)
            {
                // If the file or directory does not exist, let the user know and return an empty array.
                if (e is DirectoryNotFoundException || e is FileNotFoundException)
                {
                    ConsoleService.DisplayMessage($"Can not find {filePath}.");
                    return new string[] {};
                }
                throw;
            }
        }

        // Counts the number off occurences of the search word in the given file.
        public static int SearchForWord(string fileName, string searchWord)
        {
            string filePath = Path.Combine(resourcePath, fileName);
            string[] lines = new string[] {};

            try
            {
                lines = File.ReadAllLines(filePath);
            }
            catch (Exception e)
            {
                // If the file or directory does not exist, let the user know and return an empty array.
                if (e is DirectoryNotFoundException || e is FileNotFoundException)
                {
                    ConsoleService.DisplayMessage($"Can not find {filePath}.");
                    return -1;
                }
                throw;
            }

            // Start a timer for logging purposes.
            var watch = System.Diagnostics.Stopwatch.StartNew();

            int count = CountOccurrences(lines, searchWord);

            watch.Stop();

            // Log the search and elapsed time using the logging service.
            LoggingService.Log($"The term \"{searchWord}\" was found {count} times. The search took {watch.ElapsedMilliseconds}ms.");

            return count;
        }

        // Counts occurrences of a word in an array of strings. (ignores case)
        // Does so by iterating through each line, splitting it up into words, and counting matches.
        private static int CountOccurrences(string[] lines, string searchWord)
        {
            searchWord = CleanString(searchWord);
            int count = 0;

            // Iterate over all the lines in the file.
            foreach (string line in lines)
            {
                // Split each line and iterate over the words, clean them, and compare them to the search term.
                foreach (string word in line.Split(' '))
                {
                    string cleaned = CleanString(word);

                    if (searchWord.Equals(cleaned))
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        // Removes any characters that are not a letter or a digit, and converts to lowercase.
        private static string CleanString(string inputString)
        {
            return new string(inputString.ToLower().Where(c => char.IsLetterOrDigit(c)).ToArray());
        }
    }
}
