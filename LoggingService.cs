﻿using System;
using System.IO;
using System.Threading;

namespace File_Manager
{
    // The logging service handles logging 😮.
    // Logs are stored in the "/bin/Debug/netcoreapp3.1/logs" directory.
    static class LoggingService
    {
        private const int maxRetries = 3;
        private const int delayMilliseconds = 1000;

        private const string logsDirectoryPath = "./logs";
        private const string searchLogFilePath = "./logs/SearchLog.txt";

        private const string timeStampFormat = "yyyy-MM-dd HH:mm:ss";

        // Logs the given message alongside a timestamp to the logfile.
        public static void Log(string message)
        {
            int currentRetry = 0;

            // Using a retry pattern where the writing to file is attempted up to 3 times before throwing an error.
            for (;;)
            {
                try
                {
                    // Create the logs directory if it doesn't exist.
                    if (!Directory.Exists(logsDirectoryPath))
                    {
                        Directory.CreateDirectory(logsDirectoryPath);
                    }

                    // Open the log file and write a new log entry with a timestamp.
                    using (StreamWriter stream = File.AppendText(searchLogFilePath))
                    {
                        string timeStamp = DateTime.Now.ToString(timeStampFormat);
                        stream.WriteLine($"{timeStamp}: {message}");
                    };
                    break;
                }
                catch (IOException)
                {
                    currentRetry++;

                    // If the last retry has been reached, throw the error.
                    if (currentRetry > maxRetries)
                    {
                        throw;
                    }
                }

                // Wait for a while before retrying, so that whatever was hindering may resolv.
                Thread.Sleep(delayMilliseconds);
            }
        }
    }
}
