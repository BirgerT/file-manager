﻿using System;

namespace File_Manager
{
    // The menu service displays the menu and handles the users choises.
    static class MenuService
    {

        // Displays all the options for the user.
        public static void DisplayMainMenu()
        {
            string[] menu = new string[5] {
                "What do you want to do?",
                "quit, q: Exit the program.",
                "read, r: List all resource files.",
                "ext, e: List file types in the resourse folder.",
                "file, f: Various options to manipulate Dracula.txt"
            };

            ConsoleService.DisplayItems(menu);

            HandleMainMenuChoise(Console.ReadLine());
        }

        // Interprets main menu choise and calls appropriate methods.
        private static void HandleMainMenuChoise(string menuChoise)
        {
            switch (menuChoise)
            {
                case ("quit"):
                case ("q"):
                    ConsoleService.SayGoodbye();
                    Environment.Exit(0);
                    break;

                case ("read"):
                case ("r"):
                    ConsoleService.DisplayItems(FileService.GetResourseNames());
                    break;

                case ("ext"):
                case ("e"):
                    string[] Extensions = FileService.GetExtensions();
                    string Choise = ConsoleService.DisplayOptions(Extensions);
                    // Handle choise only if there were options.
                    if (Choise != "") ConsoleService.DisplayItems(FileService.GetResourseNames(Choise));
                    break;

                case ("file"):
                case ("f"):
                    DisplayManipulationMenu();
                    break;

                default:
                    ConsoleService.DisplayMessage("That is not a valid option.");
                    break;
            }
        }


        // Display the menu for file manipulation.
        public static void DisplayManipulationMenu()
        {
            string[] menu = new string[3]
            {
                "How do you want to manipulate Dracula.txt?",
                "info, i: List information about the file",
                "search, s: Search for word in the file"
            };

            ConsoleService.DisplayItems(menu);

            HandleManipulation(Console.ReadLine());
        }

        // Interprets the chosen option of file manipulation, and calls appropriate methods.
        public static void HandleManipulation(string choise)
        {
            string fileName = "Dracula.txt";

            switch (choise)
            {
                case ("info"):
                case ("i"):
                    string[] fileInfo = FileService.GetFileInformation(fileName);
                    ConsoleService.DisplayItems(fileInfo);
                    break;

                case ("search"):
                case ("s"):
                    ConsoleService.DisplayMessage("Input search word:");
                    string word = Console.ReadLine();
                    int count = FileService.SearchForWord(fileName, word);
                    // Display the search result only if the file exists.
                    if (count > -1) ConsoleService.DisplayMessage($"{word} was found {count} times.");
                    break;

                default:
                    ConsoleService.DisplayMessage("That is not a valid option.");
                    break;
            }
        }
    }
}
