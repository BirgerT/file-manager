# File Manager

This is my solution to the first assignment for the back-end part of Noroff .NET upskill course winter 2021:
Create a C# console application that can be used to manage and manipulate files.

## Setup
- Clone the repository
- Open it in Visual Studio
- Run the app

## Author
- Birger Topphol
