﻿using System;

namespace File_Manager
{
    class Program
    {
        static void Main()
        {
            ConsoleService.SayHello();

            // Display main menu until the user quits the program.
            while (true)
            {
                MenuService.DisplayMainMenu();
            }
        }
    }
}
