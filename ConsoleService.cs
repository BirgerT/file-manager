﻿using System;

namespace File_Manager
{
    // The console service handles anything that is displayed in the console.
    static class ConsoleService
    {
        // Display a greetings message.
        public static void SayHello()
        {
            Console.WriteLine("Welcome!");
        }

        // Display a farewell message.
        public static void SayGoodbye()
        {
            Console.WriteLine("Goodbye!");
        }

        // Display the given string.
        public static void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }

        // Display a list of items in the console.
        public static void DisplayItems(string[] list)
        {
            // Add an empty line for readability.
            Console.WriteLine();
            
            // Display all the items on separate lines.
            foreach (string item in list)
            {
                Console.WriteLine(item);
            }
        }

        // Display a list of options in the console.
        public static string DisplayOptions(string[] options)
        {
            // Add an empty line for readability.
            Console.WriteLine();

            // Display all the options on separate lines.
            for (int i = 0; i < options.Length; i++)
            {
                Console.WriteLine(i + ": " + options[i]);
            }

            // Ask the user for input only if there are any options.
            return options.Length > 0 ?
                ReceiveOptionChoise(options) : "";
        }

        // Receives a choise from an array of options.
        private static string ReceiveOptionChoise(string[] options)
        {
            bool IsNumber;
            int Option;

            // Keep asking for input until it is a number within range of the options array.
            do
            {
                IsNumber = int.TryParse(Console.ReadLine(), out Option);
            }
            while (!IsNumber || Option < 0 || Option >= options.Length);

            return options[Option];
        }

    }
}
